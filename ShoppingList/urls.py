#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app  

#This file setups top level routing
from django.conf.urls import patterns, include, url
from django.contrib import admin

#defines top level (route) path for project 
urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),# include standard django admin interface
    url(r'^api/', include('rest_api.urls')),#include  URL configuration for rest_api
    url(r'', include('frontend.urls')),#include URL configuration for static frontend website
)
