#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app  

#THis file configures URL path and routing for rest_api


from django.conf.urls import url, include
from rest_framework import routers
from rest_api import views

#rest framework proposes to configure routing this way
router = routers.DefaultRouter(trailing_slash=False) #instanciate router class

#define URLs and associated views for models
#First two routes come from rest_framework tutorial

#route to the item model
router.register(r'Items', views.ItemViewSet)
router.register(r'ShopLists', views.ShopListViewSet)
router.register(r'PredictedItems', views.PredictedItemViewSet, base_name="predicted_shoplist")



# Wire up our API using automatic URL routing.
urlpatterns = [
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^ping/$', views.Ping.as_view()),
    url(r'^', include(router.urls)),
    
    
]