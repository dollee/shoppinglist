'''
Created on Nov 9, 2014
Author: Olga Kozhevnikova
Advanced Programming Languages
CST8333_350
Assignment #2
Professor:Stanley Pieda
November 10, 2014
Purpose: to create a first working prototype for Shopping list web app  

'''
from django.db import models

'''
Custom class that is used to assemble a list of recommended items based on purchase statistic

'''
class PredictionManager(models.Manager):
    
    #this method overrides standard django models manager and is used to get related data from db
    def get_queryset(self):
        qs=super(PredictionManager, self).get_queryset()# parent class queryset
        qs.update()# flush any cached data in the queryset
	
        result=set()# prepare empty set to store keys of filtered items
        for i in qs:# (iterator) search for items we want to return and store their primary keys in the result set
            if self.needed(i):
                result.add(i.pk)
                
        return qs.filter(pk__in = result )# leave previously found items
    #helper method to test the item and decide if we need to keep it
    def needed(self,item):
        # shoplist_set is automatically created property when 
        # many-to-many relationship is declared in model and holds a reference 
        # to the set of shopping lists this item is related to 
        qs=item.shoplist_set 
        qs.update() # flush any cached data in the queryset
        count=qs.count() #count how many times an item  was bought
        if not count>1:
            return False #discard an item if never bought
        
        #determine when the item was bought for the first time
        earliest=qs.earliest("date").date
        #determine when the item was bought for the last time
        latest=qs.latest("date").date
        # calculate how often the item bought
        period=(latest-earliest)/count
        
        import datetime
        from django.utils.timezone import utc
        #determine current date
        now = datetime.datetime.utcnow().replace(tzinfo=utc)
        
        return (now - latest)>period #it was too long since we bought this item - let's put it on the list
        

