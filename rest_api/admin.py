from django.contrib import admin

from rest_api.models import Item, ShopList

admin.site.register(Item)
admin.site.register(ShopList)

