#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app 

#This file defines views for models 


from rest_api.models import Item, ShopList
from rest_framework import viewsets
from rest_api.serializers import ItemSerializer,ShopListSerializer, ShopListPutSerializer, PredictedItemSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

 
#viewsets are a collection of standard methods to perform  CRUD operations    
class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    
#viewsets are a collection of standard methods to perform  CRUD operations    
class ShopListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Items to be viewed or edited.
    """
    queryset = ShopList.objects.all()
    #override standard method to customize used viewsets for different HTTP request types. Example of a decision structure
    def get_serializer_class(self):
        if (self.request.method == "POST") or (self.request.method == "PUT") :
            return ShopListPutSerializer
        return ShopListSerializer
 
# class inherits standard viewset to represent predicted shopping list    
class PredictedItemViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows Items to be viewed or edited.
    """

    serializer_class = PredictedItemSerializer
    #override standard method to get items from custom manager
    def get_queryset(self):
       print("get predicted")
       return Item.predicted_objects.all()
    
#debug method
class Ping(APIView):
    """
    Returns a simple `pong` message when client calls `api/ping/`.
    For example:
        curl http://127.0.0.1:8000/api/ping/
    """
    def get(self, request):
        return Response('pong')
