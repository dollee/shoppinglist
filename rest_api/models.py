#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#November 10, 2014
#Purpose: to create a first working prototype for Shopping list web app 

#This file defines data objects for the application 
from django.db import models
from rest_api.prediction import PredictionManager


#define Item object
class Item(models.Model):
    itemName = models.CharField(max_length=200) #holds name of the item       
    def __str__(self):#method that return string representation of the itemname
        return self.itemName
    objects = models.Manager() # The default manager.
    predicted_objects=PredictionManager()# instantiate custom model manager
    
    

#define Shopping list object    
class ShopList(models.Model):    
    date = models.DateTimeField(auto_now=True) #hold creation timestamp
    items = models.ManyToManyField(Item) # will handle many -to-many relationship  
    def __str__(self): #method that return string representation of the  class
        return self.date.strftime("%B %d, %Y")   

    get_latest_by = "date" #specify field to sort by date
        