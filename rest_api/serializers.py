'''
Author: Olga Kozhevnikova
Advanced Programming Languages
CST8333_350
Assignment 2
Professor:Stanley Pieda
November 10, 2014
Purpose: to create a first working prototype for Shopping list web app 
'''


from rest_api.models import Item, ShopList

from rest_framework import serializers

# this file demonstrates use of serializer classes for rest_framework. 
#This classes are needed to prepare data to be served over the network 

#Serializer for the item class. It exposes two fields to the rest_api: url and itemName        
class ItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'url', 'itemName')
        
#Serializer for the item class. It exposes two fields to the rest_api: url and itemName        
class ShopListSerializer(serializers.HyperlinkedModelSerializer):
    
    items = ItemSerializer(many=True)
    name = serializers.SerializerMethodField('get_name')
    #describe object metadata
    class Meta:
        model = ShopList
        fields = ('id','name', 'url', 'date','items')
   # format date for representation
    def get_name(self, obj):
        return obj.id.__str__() + "-" + obj.date.strftime("%B %d, %Y")
    

# serializer for  shopping list model that is used for creation and update
class ShopListPutSerializer(serializers.HyperlinkedModelSerializer):
    
    items = serializers.PrimaryKeyRelatedField(many=True)
    name = serializers.SerializerMethodField('get_name')
    class Meta:
        model = ShopList
        fields = ('id','name', 'url', 'date','items')
    def get_name(self, obj):
        return obj.id.__str__() + "-" + obj.date.strftime("%B %d, %Y")

# serializer for items for predicted shopping list
class PredictedItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'itemName')


        
