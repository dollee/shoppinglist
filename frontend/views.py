#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app  


'''
This file is used for debug purpose. There were defined two views 
with different serve approaches
'''
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import render_to_response


#this view demonstrates how to write string directly to http response
def hello(request):
    return HttpResponse("Hello world")

#this view demonstrates how to render html file
def helloHtml(request):
    return render_to_response('frontend/hello.html', RequestContext(request),)
