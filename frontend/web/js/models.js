/*
 * http://sauceio.com/index.php/2014/07/angularjs-data-models-http-vs-resource-vs-restangular/
 */

angular.module('item.models', ['ngResource'])
    .factory('Item', ['$resource', function($resource) {
        var Item = $resource('/api/Items/:itemId', {itemId:'@id'}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
 
        Item.prototype.getResult = function() {
            if (this.status == 'complete') {
                if (this.passed === null) return "Finished";
                else if (this.passed === true) return "Pass";
                else if (this.passed === false) return "Fail";
            }
            else return "Running";
        };
 
        return Item;
    }]);

angular.module('item.models')
.factory('PredictedItems', ['$resource', function($resource) {
    var r = $resource('/api/PredictedItems/:itemId', {itemId:'@id'}, {
        query: {
            method: 'GET',
            isArray: false
        }
    });

    r.prototype.getResult = function() {
        if (this.status == 'complete') {
            if (this.passed === null) return "Finished";
            else if (this.passed === true) return "Pass";
            else if (this.passed === false) return "Fail";
        }
        else return "Running";
    };

    return r;
}]);



angular.module('item.models')
.factory('ShopList', ['$resource', function($resource) {
    var r = $resource('/api/ShopLists/:itemId', {itemId:'@id'}, {
        query: {
            method: 'GET',
            isArray: false
        }
    });

    r.prototype.getResult = function() {
        if (this.status == 'complete') {
            if (this.passed === null) return "Finished";
            else if (this.passed === true) return "Pass";
            else if (this.passed === false) return "Fail";
        }
        else return "Running";
    };

    return r;
}]);