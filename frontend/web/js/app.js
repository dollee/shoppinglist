"use strict";

var App = angular.module("example",['item.models']);

App.controller("Ctrl",['$scope', 'Item','ShopList','PredictedItems', function  ($scope, Item, ShopList, PredictedItems) {
	//
	$scope.name="Buy today";
	$scope.modeEdit=false;
	$scope.newItem="";
	$scope.AddItem=function(){
		if ($scope.newItem != "") {
			 var item = new Item();
			 item.itemName=$scope.newItem;
			 item.$save();
		      $scope.shopList.push(item);
		      $scope.newItem = "";
		    }
	}
	
	$scope.selectedItem=null;
	$scope.addToList=function(){
		if ($scope.selectedItem!=null){
			$scope.shopList.push($scope.selectedItem);
			$scope.selectedItem=null;
		}
	}
	
    $scope.loadItems = function() {
        var items = Item.query({}, function(items) {
            $scope.items = items.results;
         });
    };

    $scope.loadItems();

    $scope.loadList = function() {
        var items = PredictedItems.query({}, function(items) {
            $scope.shopList = items.results;
         });
    };

    $scope.loadList();
    
    $scope.saveList=function(){
    	var l = new ShopList();
    	l.items=[];
    	$scope.shopList.forEach(function(i){
    		if(i.checked){
    			l.items.push(i.id);
    		}
    	});
    	l.$save();
	$scope.loadList();
    };
    $scope.refresh=function(){
       $scope.loadList();
       $scope.loadItems();
    }

    
	
}]);

App.filter('notInList',function(){
  return function(items,list){
	  var result=[];
	  items.forEach(function(i){
		  if(
			list.every(function(k){
				return i.id!==k.id;
			}))
		  {
			  result.push(i);
		  }
	  });
	  
	  return result;
	  };
})
