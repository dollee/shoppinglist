#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app  

#This file specifies routing rules for static page (index.html) and 
#its assets (javascript and css files)
 
 
 #import app_setting module that holds app-wide settings
from frontend import app_settings

#include system dependencies
from django.conf.urls import patterns, url
from django.views.static import serve

# setup routing for front end. For empty URL path serve index.html. Served from the folder that 
#specified in 'document root' (see app_settings.py )
urlpatterns = patterns('',
    url(r'^$',serve,{
        'document_root' : app_settings.content_dir(),
        'path' : 'index.html',},
    ),
    url(r'^(?P<path>.+)$',serve,{
        'document_root' : app_settings.content_dir(),},
    ),                       
)
