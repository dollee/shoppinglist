#Author: Olga Kozhevnikova
#Advanced Programming Languages
#CST8333_350
#Assignment #2
#Professor:Stanley Pieda
#October 20, 2014
#Purpose: to create a first working prototype for Shopping list web app  


 #import app_setting module that holds app-wide settings
import os
from django.conf import settings

#helper variable which holds full path to application directory
APP_DIR = os.path.join(getattr(settings, 'BASE_DIR'), 'frontend/')

#function to return full system path to web frontend interface
def content_dir():
    return os.path.join(APP_DIR, 'web/')